package com.techu.apitechudb.repositories;

import com.techu.apitechudb.models.UserModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends MongoRepository<UserModel, String> {
    /*Para Ordenar solo se agrega la palabra ByOrderBy{Parametro}
    * esto se hace por convencion de spring*/
    List<UserModel> findByOrderByAgeAsc();
}
