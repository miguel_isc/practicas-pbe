package com.techu.apitechudb.services;

import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {
    @Autowired
    UserRepository userRepository;

    public List<UserModel> findAll(boolean orderAge) {
        System.out.println("findAll UserService");

        return  orderAge? this.userRepository.findByOrderByAgeAsc():this.userRepository.findAll();

        /*Se puede utilizar lo siguiente si no se crea el metodo en el repository
        * */
        //return this.userRepository.findAll(Sort.by(Sort.Direction.DESC, "age"));
    }

    public Optional<UserModel> findById(String id) {
        System.out.println("find UserService");
        return this.userRepository.findById(id);
    }

    public UserModel add(UserModel userModel){
        System.out.println("add en UserService");

        return this.userRepository.save(userModel);
    }

    public boolean update(UserModel userModel, String id){
        System.out.println("update en UserService");
        boolean result = false;


        if(this.findById(id).isPresent() == true){
            System.out.println("User encontrado y borrando");
            userModel.setId(id);
            this.userRepository.save(userModel);
            result = true;
        }

        return result;
    }

    public boolean delete(String id){
        System.out.println("delete en UserService");
        boolean result = false;

        if(this.findById(id).isPresent() == true){
            System.out.println("User encontrado y borrando");
            this.userRepository.deleteById(id);
            result = true;
        }

        return result;
    }
}
