package com.techu.apitechudb.controllers;

import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
public class UserController {

    @Autowired
    UserService userService;

    /*
     * Definiendo una combinación de URL y método http acorde con REST, preparar las siguientes peticiones:
     * · Obtener todos los usuarios, pudiendo ordenarlos por su edad (vale cualquier dirección de la ordenación).
     *
     *   NOTA - Utilizar un parámetro de Query String opcional para pedir ordenar en base a la edad.
     *   PISTA - Buscar cómo ordenar en Spring Data.
     * */
    @GetMapping("/users")
    public ResponseEntity<List<UserModel>> getUsers(@RequestParam(value = "orderAge", defaultValue = "false") boolean orderAge){
        System.out.println("getUsers");
        return new ResponseEntity<>(
                this.userService.findAll(orderAge), HttpStatus.OK
        );
    }

    /*
     * Obtener un usuario en base a su id.
     * */
    @GetMapping("/users/{id}")
    public ResponseEntity<Object> getUserById(@PathVariable String id){
        System.out.println("getUserById");

        Optional<UserModel> result = this.userService.findById(id);

        return new ResponseEntity<>(
                result.isPresent()?result.get():"User no encontrado",
                result.isPresent()?HttpStatus.OK: HttpStatus.NOT_FOUND
        );
    }

    /*
     * Crear un usuario.
     * */
    @PostMapping("/users")
    public ResponseEntity<UserModel> addUser(@RequestBody UserModel userModel){
        System.out.println("addUser");

        return new ResponseEntity<>(
                this.userService.add(userModel), HttpStatus.CREATED
        );

    }

    /*
     * Actualizar completamente un usuario (menos la id) en base a su id.
     * */
    @PutMapping("/users/{id}")
    public ResponseEntity<UserModel> updateUser(@RequestBody UserModel userModel, @PathVariable String id){
        System.out.println("updateUser");

        boolean userToUpdate = this.userService.update(userModel, id);

        return new ResponseEntity<>(
                userModel,
                userToUpdate? HttpStatus.OK: HttpStatus.NOT_FOUND
        );
    }


    /*
     * Borrar un usuario en base a su id.
     * */
    @DeleteMapping("/users/{id}")
    public ResponseEntity<String> deleteUser(@PathVariable String id){
        System.out.println("deleteUser");

        boolean deleteUser = this.userService.delete(id);

        return new ResponseEntity<>(
                deleteUser? "User Borrado" : "User No Borrado",
                deleteUser? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }
}
