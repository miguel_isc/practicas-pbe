package com.techu.apitechudb.controllers;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
public class ProductController {

    @Autowired
    ProductService productService;

    @GetMapping("/products")
    public ResponseEntity< List<ProductModel>> getProducts(){
        System.out.println("getProducts");
        return new ResponseEntity<>(
                this.productService.findAll(), HttpStatus.OK
        );
    }

    @GetMapping("/products/{id}")
    public ResponseEntity<Object> getProductById(@PathVariable String id){
        System.out.println("getProductById");

        Optional<ProductModel> result = this.productService.findById(id);

        return new ResponseEntity<>(
                result.isPresent()?result.get():"Producto no encontrado",
                result.isPresent()?HttpStatus.OK: HttpStatus.NOT_FOUND
        );
    }

    @PostMapping("/products")
    public ResponseEntity<ProductModel> addProduct(@RequestBody ProductModel productModel){
        System.out.println("addProducts");

        return new ResponseEntity<>(
                this.productService.add(productModel), HttpStatus.CREATED
        );

    }

    @PutMapping("/products/{id}")
    public ResponseEntity<ProductModel> updateProduct(@RequestBody ProductModel productModel, @PathVariable String id){
        System.out.println("updateProduct");

        Optional<ProductModel> productToUpdate = this.productService.findById(id);

        if(productToUpdate.isPresent()){
            System.out.println("Producto para actualizar encontrado, actualizando");
            this.productService.update(productModel);
        }

        return new ResponseEntity<>(
                productModel,
                productToUpdate.isPresent()? HttpStatus.OK: HttpStatus.NOT_FOUND
        );
    }


    @DeleteMapping("/products/{id}")
    public ResponseEntity<String> deleteProduct(@PathVariable String id){
        System.out.println("deleteProduct");

        boolean deleteProduct = this.productService.delete(id);

        return new ResponseEntity<>(
                deleteProduct? "Producto Borrado" : "Producto No Borrado",
                deleteProduct? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }
}
