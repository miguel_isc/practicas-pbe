package com.techu.apitechu.controllers;

import com.techu.apitechu.ApitechuApplication;
import com.techu.apitechu.models.ProductModel;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.ArrayList;
import java.util.List;

@RestController
public class ProductController {
    static final String APIBaseUrl = "/apitechu/v1";

    @GetMapping(APIBaseUrl+"/products")
    public ArrayList<ProductModel> getProducts(){
        System.out.println("getProducts");

        return ApitechuApplication.productModels;
    }

    @GetMapping(APIBaseUrl+"/products/{id}")
    public ProductModel getProductById(@PathVariable String id){
        System.out.println("getProductById");
        System.out.println("Id es: " + id);

        ProductModel result = new ProductModel();

        for(ProductModel product : ApitechuApplication.productModels){
            if(product.getId().equals(id)){
                result = product;
            }
        }

        return result;
    }

    @PostMapping(APIBaseUrl+"/products")
    public ProductModel createProduct(@RequestBody ProductModel newProduct){
        System.out.println("createProduct");
        System.out.println("La id del nuevo producto es: "+newProduct.getId());

        ApitechuApplication.productModels.add(newProduct);

        return newProduct;
    }

    @PutMapping(APIBaseUrl+"/products/{id}")
    public ProductModel updateProduct(@RequestBody ProductModel product, @PathVariable String id){
        System.out.println("updateProduct");

        for(ProductModel productInList : ApitechuApplication.productModels){
            if(productInList.getId().equals(id)){
                productInList.setId(product.getId());
                productInList.setDesc(product.getDesc());
                productInList.setPrice(product.getPrice());
            }
        }

        return product;
    }

    @DeleteMapping(APIBaseUrl+"/products/{id}")
    public ProductModel deleteProduct(@PathVariable String id){
        System.out.println("deleteProduct");

        ProductModel result= new ProductModel();
        boolean foundCompany = false;

        for(ProductModel productInList : ApitechuApplication.productModels){
            if(productInList.getId().equals(id)){
                foundCompany = true;
                result = productInList;
            }
        }

        if(foundCompany){
            ApitechuApplication.productModels.remove(result);
        }

        return result;
    }

    @PatchMapping (APIBaseUrl+"/products/{id}")
    public ProductModel patchProduct(@RequestBody ProductModel productData, @PathVariable String id){
        System.out.println("patchProduct");

        ProductModel result= new ProductModel();

        for(ProductModel productInList : ApitechuApplication.productModels){
            if(productInList.getId().equals(id)){
                result = productInList;


                if(productData.getDesc()!=null){
                    productInList.setDesc(productData.getDesc());
                }

                if(productData.getPrice()>0){
                    productInList.setPrice(productData.getPrice());
                }
            }

        }

        return result;
    }
}
